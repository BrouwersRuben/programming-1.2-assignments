package be.kdg.prog12.tiles;

import be.kdg.prog12.tiles.model.TileModel;
import be.kdg.prog12.tiles.view.Presenter;
import be.kdg.prog12.tiles.view.TileView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        final TileModel model = new TileModel();
        final TileView view = new TileView();
        new Presenter(model, view);

        primaryStage.setTitle("Tiles");
        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }
}
