package be.kdg.prog12.mapDrivers;

import java.util.*;

public class Drivers {
    Map<String, Driver> driverByName = new HashMap<>();
    Map<Integer, Driver> driverByYear = new HashMap<>();

    public Drivers(Map<String, Driver> driverByName, Map<Integer, Driver> driverByYear) {
        this.driverByName = driverByName;
        this.driverByYear = driverByYear;
    }

    public Drivers(String[][] champions, Integer[][] years) {
        for (int i = 0; i < champions.length; i++) {
            Driver driver = new Driver(champions[i][0], champions[i][1]);
            for (Integer year : years[i]) {
                driver.setYearsTheDriverWasChampion(year);
                driverByYear.put(year, driver);
            }
            driverByName.put(driver.getName(), driver);
        }
    }

    public Driver getDriverByName(String name) {
        return (driverByName.get(name));
    }

    public Driver getDriverByYear(int year) {
        return driverByYear.get(year);
    }

    public Collection<Driver> getDrivers() {
        return driverByName.values();
    }

    @Override
    public String toString() {
        return String.format("Drivers by name: \n%s\nDrivers by year: \n%s", driverByName, driverByYear);
    }

}
