package retire.view;

import retire.model.Retirement;

public class RetirementPresenter {
    private final Retirement model;
    private final RetirementView view;

    public RetirementPresenter(Retirement model, RetirementView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        this.view.getButton().setOnAction(event -> {
            try {
                model.setBirthYear(Integer.parseInt(view.getInputField().getText()));
                updateView();
            } catch (NumberFormatException e) {
                this.view.getOutputField().setText("invalid");
            }
        });
    }

    private void updateView() {
        //TODO: 67 shows in the output field on startup of the program.
        this.view.getOutputField().setText(Integer.toString(this.model.getRetirementYear()));
    }
}