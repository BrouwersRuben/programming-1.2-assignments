package retire.model;

public class Retirement {

    private final int RETIREMENT_AGE = 67;
    // private attributes
    private int birthYear;

    public Retirement() {
        // Constructor
    }

    // methods with business logic
    public int getRetirementYear() {
        return birthYear + RETIREMENT_AGE;
    }

    // needed getters and setters
    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
}
