package be.kdg.prog12.m5.main.java.model;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Survey {
    // private attributes
    private final Map<String, Integer> pieChartData = new HashMap<>();

    public Survey() {
    // Constructor
    }

    // methods with business logic
    public void setEntry(String answer, int percentage){
        pieChartData.put(answer, percentage);
    }

    // needed getters and setters
    public Map<String, Integer> getPieChartData() {
        return pieChartData;
    }
}
