package appname;

import appname.model.AppNameModel;
import appname.view.AppNamePresenter;
import appname.view.AppNameView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import static javafx.application.Application.launch;


public class Main extends Application {

    public static void main(String[] args){launch(args);}

    @Override
    public void start(Stage primaryStage) {
        AppNameModel model = new AppNameModel();
        AppNameView view = new AppNameView();
        new AppNamePresenter(model, view);
        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }
}
