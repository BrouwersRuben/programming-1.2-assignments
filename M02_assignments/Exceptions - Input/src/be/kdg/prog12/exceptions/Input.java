package be.kdg.prog12.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Input {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        double userInput = 0;
        boolean userInputCor = false;

        while (!userInputCor){
            System.out.print("Please enter a number from 1 to 10: ");
            try {
                userInput = sc.nextInt();
                if (userInput >= 1 && userInput <= 10) {
                    userInputCor = true;
                } else {
                    throw new InputMismatchException(String.format("%.0f is not a number from 1 to 10.",userInput));
                }
            } catch (InputMismatchException e) {
                //System.err.println(e.getClass());
                //System.out.printf("%.0f is not a number from 1 to 10.\n", userInput);
                System.out.println(e.getMessage());
                sc.nextLine();
            }
        }

        System.out.printf("%.0f is a valid number!", userInput);

    }
}
