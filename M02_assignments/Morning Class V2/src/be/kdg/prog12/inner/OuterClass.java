package be.kdg.prog12.inner;

public class OuterClass {
    private int value;
    private String stringValue;

    void takeSomeOtherAction(){
        InnerClass ic = this.new InnerClass();
        ic.innerValue++;
    }

    //Regular: so it is tied to an instance of the outer
    public class InnerClass{
        private int  innerValue;

        void takeAction(){
            innerValue++;
            value++;
        }
    }
}
