package be.kdg.prog12.inner;

public class Car {
    private String brand;
    private String model;
    private Color color;

    public enum Color{
        BLACK, BLUE, WHITE
    }

    public interface Printable{

    }

    public Color getColor(){
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
