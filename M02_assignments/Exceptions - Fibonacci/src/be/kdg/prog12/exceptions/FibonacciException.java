package be.kdg.prog12.exceptions;

public class FibonacciException extends ArithmeticException{
//    private String message;

    public FibonacciException(String message) {
        super(message);
    }
/*
    public FibonacciException(String s, String message) {
        super(s);
        this.message = message;
    }
 */
}
