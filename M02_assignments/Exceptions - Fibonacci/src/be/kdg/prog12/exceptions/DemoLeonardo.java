package be.kdg.prog12.exceptions;

public class DemoLeonardo {
    public static void main(String[] args) {
        try {
            Fibonacci.finonacciNumber(-1);
        } catch (FibonacciException e){
            System.err.println(e.getMessage());
        }

        try{
        for (int i = 0; i < 100; i++) {
            double dividend = Fibonacci.finonacciNumber(i + 1);
            long divisor = Fibonacci.finonacciNumber(i);
            System.out.printf("f(%d) / f(%d) = %.15f%n", i + 1,
                    i, dividend / divisor);
        }
        } catch (FibonacciException e){
            System.err.println(e.getMessage());
        }
    }
}
