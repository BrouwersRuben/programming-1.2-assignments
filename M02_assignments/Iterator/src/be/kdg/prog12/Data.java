package be.kdg.prog12;

public class Data {
    private final static int MAX = 15;
    private final int[] numbers = new int[MAX];

    public Data() {
        for (int i = 0; i < MAX; i++) {
            numbers[i] = (i + 10);
        }
    }

    class Iterator {
        int next = 0;

        public boolean hasNext(){
            return next < MAX;
            /*
            if (next <= MAX){
                return true;
            } else {
                return false;
            }
             */
        }

        public int next(){
            return numbers[next++];
            /*
            next++;
            return numbers[next-1];
             */
        }
    }
}


