package be.kdg.prog12.enums;

public enum Audio {
    PCM, DOLBY, DOLBY_HD, VHS, DTS_HD;


    @Override
    public String toString() {
        if(this == DOLBY) {
            return "Dolby";
        } else if (this == DOLBY_HD) {
            return "Dolby HD";
        } else {
            return name();
        }
    }
    /*
    @Override
    public String toString() {
        switch (this) {
            case PCM:
                return "PCM";
            case DOLBY:
                return "Dolby";
            case DOLBY_HD:
                return "Dolby HD";
            case VHS:
                return "VHS";
            case DTS_HD:
                return "DTS HD";
            default:
                return null;
        }
    }

     */
}
