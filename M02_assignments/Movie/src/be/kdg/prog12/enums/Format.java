package be.kdg.prog12.enums;

public enum Format {
    VHS, DVD, BLU_RAY;

    @Override
    public String toString() {
        if(this == BLU_RAY) {
            return "Blu-ray";
        } else {
            return name();
        }
    }

    /*
    @Override
    public String toString() {
        switch (this) {
            case VHS:
                return "VHS";
            case DVD:
                return "DVD";
            case BLU_RAY:
                return "Blu-ray";
            default:
                return null;
        }
    }

     */
}
