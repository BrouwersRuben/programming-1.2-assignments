package be.kdg.prog12.exceptions;

public class Book extends Product{
    private String author;
    private String title;


    public Book(String code, String description, double price, String author, String title) throws ECommerceException {
        super(code, description, price);
        this.author = author;
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                super.toString()+
                '}';
    }
}
