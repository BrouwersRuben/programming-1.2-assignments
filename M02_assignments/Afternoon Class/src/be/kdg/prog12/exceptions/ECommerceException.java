package be.kdg.prog12.exceptions;

public class ECommerceException extends Exception{
    private String code;

    public ECommerceException(String s, String code){
        super(s);
        this.code = code;
    }

    public ECommerceException(){
        super();
    }

    public String getCode(){
        return code;
    }
}
