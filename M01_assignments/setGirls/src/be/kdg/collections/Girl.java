package be.kdg.collections;

import java.util.Objects;

public class Girl {
    String name;
    double age;

    @Override
    public String toString() {
        return String.format("%s(%.0f)", name, age);
    }

    public Girl(String name, double age) {
        this.name = name;
        this.age = age;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Girl)) return false;
        Girl girl = (Girl) o;
        return Double.compare(girl.age, age) == 0 && name.equals(girl.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age);
    }

    /*
    Task 2: add equals/hashcode methods
        Add equals/hashcode methods

            based on age
            based on name
            based an name and age
            
        Print out each time an observe the differences.
     */
}
