package be.kdg.collections;

import java.util.*;

public class Runner {
    public static void main(String[] args) {
        List<Girl> list = new LinkedList<>(List.of(new Girl("An", 20),new Girl("Bea", 20),
                new Girl("Bea", 25), new Girl("Diana", 25),
                new Girl("Zoë", 18), new Girl("Ekaterina", 18),
                new Girl("Bea", 20)));

        /*
        girls.add(new Girl("An", 20));
        girls.add(new Girl("Bea", 20));
        girls.add(new Girl("Bea", 25));
        girls.add(new Girl("Diana", 25));
        girls.add(new Girl("Zoë", 18));
        girls.add(new Girl("Ekaterina", 18));
        girls.add(new Girl("Bea", 20));
        */

        for (Girl girl : list) {
            System.out.println(girl);
        }
        System.out.println();

        Set<Girl> set = new HashSet<>(list);

        System.out.printf("%d drivers: %s", set.size(), set);
    }
}
