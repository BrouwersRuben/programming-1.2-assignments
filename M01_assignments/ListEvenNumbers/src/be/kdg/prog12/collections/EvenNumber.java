package be.kdg.prog12.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class EvenNumber {
    public static void main(String[] args) {
        Random rd = new Random();
        List<Integer> numberList = new ArrayList<>();

        for (int i = 0; i < 20; i ++){
            numberList.add(rd.nextInt(50)+1);
        }

        for (Integer numb : numberList){
            System.out.print(numb + " ");
        }

        System.out.println();

        int[] numberArray = new int[numberList.size()];

        for(int i = 0; i < numberList.size(); i++){
            numberArray[i]  = numberList.get(i);
        }

        for (Object o : numberArray) {
            System.out.print(o + " ");
        }

        System.out.println();

        List<Integer> evenNumbers = new ArrayList<>();

        for(int i = 0; i < numberArray.length; i++){
            evenNumbers.add(numberArray[i]);
        }

        for (int i = 0; i < evenNumbers.size(); i++)
        {
            if (evenNumbers.get(i) % 2 != 0 ){
                evenNumbers.remove(i);
                i--;
            }
        }

        System.out.println(evenNumbers);

        Collections.sort(evenNumbers);
        System.out.println(evenNumbers);

        Collections.reverse(evenNumbers);
        System.out.println(evenNumbers);

        Collections.shuffle(evenNumbers);
        System.out.println(evenNumbers);

        System.out.println(Collections.frequency(evenNumbers, 48));
    }
}
