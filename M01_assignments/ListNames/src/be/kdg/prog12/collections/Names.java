package be.kdg.prog12.collections;

import java.util.ArrayList;
import java.util.List;

public class Names {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();

        names.add("Albert");
        names.add("Henry");
        names.add("Josephine");
        names.add("Annabelle");
        names.add("Ashraf");
        //names.add("Georgie");

        System.out.println();

        System.out.println(names.get(0));
        System.out.println(names.get(names.size()-1));

        System.out.println();

        /*
        for (int i = 0; i < names.size(); i++){
            System.out.println(names.get(i));
        }
        */

        for (String name: names) {
            System.out.println(name);
        }

        System.out.println();

        /*
        for (int i = 0; i < names.size(); i++){
            if(names.get(i) == "Georgie"){
                System.out.println("Georgie is in the list!");
                break;
            }
        }
        */

        if (names.contains("Georgie")){
            System.out.println("Georgie is in the list");
        } else {
            System.out.println("Georgie is not in the list");
        }

        System.out.println();

        for (int i = 0; i < names.size(); i++){
            if (names.get(i).startsWith("A")){
                names.remove(i);
                i--;
            }
        }

        for (String name: names) {
            System.out.println(name);
        }

        //or

        System.out.println(names);
    }
}
