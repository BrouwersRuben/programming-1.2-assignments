package collections;

import java.util.*;

/**
 * Author: Jan de Rijke
 */
public class Runner {
	public static void main(String[] args) {

		List<Girl> listGirls = List.of(new Girl("An", 20),new Girl("Bea", 20),
			new Girl("Bea", 25), new Girl("Diana", 25),
			new Girl("Zoë", 18), new Girl("Ekaterina", 18),
			new Girl("Bea", 20));

		Map<String, Girl> mapGirls = new TreeMap<>();
		Map<String, Girl> mapGirlsHash = new HashMap<String, Girl>();

		for (Girl girl : listGirls) {
			mapGirlsHash.put(girl.getName(), girl);
		}

		for (Girl girl : listGirls) {
			mapGirls.put(girl.getName(), girl);
		}

		System.out.printf("Map of %d girls: %s\n",mapGirls.size(), mapGirls);
		System.out.printf("Map of %d girls: %s",mapGirlsHash.size(), mapGirlsHash);
	}
}
