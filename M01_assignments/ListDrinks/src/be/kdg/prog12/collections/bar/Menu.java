package be.kdg.prog12.collections.bar;

import java.util.*;

public class Menu{
    List<Drink> drinks = new ArrayList<>();

    public List<Drink> getDrinks(){
        return drinks;
    }

    public void addDrinks(Drink d){
        drinks.add(d);
    }

    public int getSize(){
        return drinks.size();
    }

    public double getTotalPrice(){
        double total = 0;
        for (Drink drink : drinks) {
            total += drink.getPrice();
        }
        return total;
    }

    //TODO: HELP?!?!
    public Drink mostExpensiveDrink(){
        return Collections.max(drinks, Comparator.comparing(Drink::getPrice));
    }

    public List<Drink> getAlcoholicDrinks() {

        List<Drink> alcoholic = new ArrayList();

        for(Drink d : drinks) {
            if (d.isAlcoholic()) {
                alcoholic.add(d);
            }
        }
        return alcoholic;
    }

    //TODO: Crying inside
    public void removeMoreExpensiveThen(int amount){
        drinks.removeIf(drink -> drink.getPrice() > amount);
    }
}
