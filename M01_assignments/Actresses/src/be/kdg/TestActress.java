package be.kdg;

import java.util.*;

public class TestActress {
    private final static  List<Actress> testdata = List.of(
            new Actress("Cameron Diaz", 1972),
            new Actress("Nathalie Meskens", 1982),
            new Actress("Angelina Jolie", 1975),
            new Actress("Jennifer Lopez", 1970),
            new Actress("Reese Witherspoon", 1976),
            new Actress("Zoe Kravitz", 1988),
            new Actress("Jennifer Lawrence", 1990),
            new Actress("Kirsten Dunst", 1982),
            new Actress("Kate Winslet", 1975),
            new Actress("Emma Watson", 1990),
            new Actress("Marie Vinck", 1983),
            new Actress("Anne Hathaway", 1982),
            new Actress("Drew Barrymore", 1975),
            new Actress("Natalie Portman", 1981),
            new Actress("Tara Reid", 1975),
            new Actress("Katie Holmes", 1978),
            new Actress("Anna Faris", 1976)
    );

    public static void main(String[] args) {
        Actress reese = new Actress("Reese Witherspoon", 1976);
        Actress drew = new Actress("Drew Barrymore", 1975);
        Actress anna = new Actress("Anna Faris", 1976);
        Actress nathalie= new Actress("Nathalie Meskens", 1982);

        //Print out the size of the test Data
        System.out.printf("Starting list of %d\n", testdata.size());

        System.out.println();

        // Use a List to add drew and anna to the test data. Print out the size of the list.
        // Print out the list with a for-each statement

        List<Actress> list=new ArrayList<>(testdata);

        list.add(drew);
        list.add(anna);

        for (Actress actresses : list) {
            System.out.print(actresses + ", ");
        }

        System.out.println();

        //next remove reese and thandie from the list and print the list size and the list with one statement

        list.remove(reese);
        list.remove(nathalie);

        System.out.printf("\nList of %d with reese and nathalie removed %s\n", list.size(), list);


        // now remove all actresses born before 1976 and print again

        //TODO: ITERATOR!!
        Iterator<Actress> removeIT = list.iterator();

        while(removeIT.hasNext()){
            Actress actress = removeIT.next();
            if (actress.getBirthYear() < 1976){
                removeIT.remove();
            }
        }

        System.out.printf("\nList of %d with everyone born before 1976 removed %s\n", list.size(), list);


        // use an appropriate collection to remove all duplicates from the resulting list
        // and sort in natural order. Print out the collection

        Set<Actress> orderedActress = new TreeSet<>(list);

        System.out.printf("Sorted collection of %d without duplicates: %s", orderedActress.size(), orderedActress);
    }
}
