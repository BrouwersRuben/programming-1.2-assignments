package be.kdg;

import be.kdg.merken.Brands;


public class TestBrands {
	public static void main(String[] args) {
		Brands brands = new Brands();
		System.out.println(brands);

		brands.alphabetic();
		System.out.println(brands);
		brands.alphabeticDescending();
		System.out.println(brands);
	}
}

/*
[BMW, Audi, VW, Ford, Opel, Renault, Peugeot, Citroen, Mercedes, Fiat]
[Audi, BMW, Citroen, Fiat, Ford, Mercedes, Opel, Peugeot, Renault, VW]
[VW, Renault, Peugeot, Opel, Mercedes, Ford, Fiat, Citroen, BMW, Audi]
*/