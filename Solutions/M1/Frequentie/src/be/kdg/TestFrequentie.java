package be.kdg;

import be.kdg.frequentie.Data;
import be.kdg.frequentie.Frequentie;

/*
Gebruik deze klasse om je programma te testen. Je mag hier niets aan wijzigen!
 */
public class TestFrequentie {
    public static void main(String[] args) {

        Data data = new Data();
        Frequentie frequentie = new Frequentie(data.getGetallen());

        System.out.println("Inhoud tabel:\n" + frequentie);
        System.out.println("Frequenties:\n" + frequentie.toonFrequenties());
    }
}

/*
    Inhoud tabel:
    3 0 8 5 9 5 6 9 7 9
    8 1 6 9 1 0 7 9 5 6
    4 7 3 5 9 9 3 1 7 9
    0 1 3 2 0 1 1 6 5 6
    7 5 6 2 7 1 2 7 3 9
    2 5 8 4 3 4 3 8 5 6
    0 8 2 3 3 6 4 0 2 6
    0 4 6 6 3 6 2 8 0 7
    1 9 0 5 1 4 8 0 4 4 
    9 0 6 8 3 3 4 6 9 6

    Frequenties:
    0 --> 11
    1 -->  9
    2 -->  7
    3 --> 12
    4 -->  9
    5 -->  9
    6 --> 15
    7 -->  8
    8 -->  8
    9 --> 12
*/