package be.kdg.collections;

import java.util.*;

/**
 * Author: Jan de Rijke
 */
public class Runner {
	public static void main(String[] args) {

		List<Girl> listGirls = List.of(new Girl("An", 20),new Girl("Bea", 20),
			new Girl("Bea", 25), new Girl("Diana", 25),
			new Girl("Zoë", 18), new Girl("Ekaterina", 18),
			new Girl("Bea", 20));
		Set<Girl> setGirls = new HashSet<>(listGirls);
		System.out.printf("Set of %d girls: %s",setGirls.size(), setGirls);
	}
}
