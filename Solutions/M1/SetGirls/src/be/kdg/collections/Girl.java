package be.kdg.collections;

import java.util.Objects;

/**
 * Author: Jan de Rijke
 */
public class Girl {
	private String name;
	private int age;

	public Girl(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Girl)) return false;
		Girl girl = (Girl) o;
		return age == girl.age ;
			//&& Objects.equals(name, girl.name);
	}

	@Override
	public int hashCode() {
		return age;
 		//Objects.hash(name, age);
	}

	@Override
	public String toString() {
		return String.format("%s (%d)",name,age);
	}
}
