package be.kdg.collections;


import java.util.Random;
import java.util.TreeSet;

/**
 * De klasse TestLottoGetallen maakt via de constructor een nieuw LottoGetallen-object.
 * Voer 40 keer na mekaar de methode genereerLottoGetallen uit en
 * toon de getallen telkens op één regel door gebruik te maken van de methode toonLottoGetallen.
 */
public class LottoDraw {
    public static void main(String[] args) {
        TreeSet<Integer> numbers = new TreeSet<>();
        Random random = new Random();
        do{
            numbers.add(random.nextInt(45)+1);
        }while(numbers.size()<6);
        System.out.print("The lotto numbers for today are:");
        for (Integer number : numbers) {
            System.out.print(" " + number);
        }
    }
}

