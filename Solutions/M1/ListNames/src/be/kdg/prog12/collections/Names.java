package be.kdg.prog12.collections;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Author: Jan de Rijke
 */
public class Names {

	public static void main(String[] args) {
		ArrayList<String> namen = new ArrayList<>();
		namen.add("Albert");
		namen.add("Henry");
		namen.add("Josephine");
		namen.add("Annabelle");
		namen.add("Ashraf");
		//print first:
		System.out.println(namen.get(0));
		//print last:
		System.out.println(namen.get(namen.size() - 1));
		//for each :
		System.out.print("All names: ");
		for (String naam : namen) {
			System.out.print(naam + " ");
		}
		//Is Georgie in the list?
		System.out.println("\nGeorgie :" + namen.contains("Georgie"));

		//Remove all names starting with A
		//Use an iterator!
		//for each is read-only!!!
		Iterator<String> it = namen.iterator();
		while (it.hasNext()) {
			if (it.next().startsWith("A")) {
				it.remove();
			}
		}

		//Print again
		System.out.print("Selected names: ");
		System.out.println(namen);
	}
}

