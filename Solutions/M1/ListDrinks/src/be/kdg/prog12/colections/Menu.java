package be.kdg.prog12.colections;

import java.util.*;

/**
 * Author: Jan de Rijke
 */
public class Menu {
	private List<Drink> drinks = new LinkedList<>();

	public void addDrink(Drink drink){
		drinks.add(drink);
	}

	public int getSize(){
		return drinks.size();
	}

	public double sumPrices(){
		double sum = 0.0;
		for (Drink drink : drinks) {
			sum+=drink.getPrice();
		}
		return sum;
	}

	@Override
	public String toString() {
		return "Menu: [" +
			"drinks=" + drinks +
			']';
	}

	public Drink mostExpensive(){
		Drink mostExpensive=drinks.get(0);
		for (Drink drink : drinks) {
			if (drink.getPrice() > mostExpensive.getPrice()) mostExpensive=drink;
		}
		return mostExpensive;
	}

	public List<Drink> getAlcoholicDrinks(){
		List<Drink> alcoholic = new LinkedList<>();
		for (Drink drink : drinks) {
			if (drink.isAlcoholic()) alcoholic.add(drink);
		}
		return alcoholic;
	}

	public void removeMoreExpensiveThen(double price){
		for(Iterator<Drink> it = drinks.iterator(); it.hasNext();){
			if (it.next().getPrice() > price) it.remove();
		}
	}

	public void addDrinks(Drink[] drinksRa){
		drinks.addAll(Arrays.asList(drinksRa));
	}


}
