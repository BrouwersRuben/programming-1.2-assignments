package be.kdg.prog12.colections;

/**
 * Author: Jan de Rijke
 */
public class Drink implements Comparable<Drink>{
	private String name;
	private double price;
	private boolean isAlcoholic;

	@Override
	public String toString() {
		return String.format("%s €%.2f",name,price);
	}

	public Drink(String name, double price, boolean isAlcoholic) {
		this.name = name;
		this.price = price;
		this.isAlcoholic = isAlcoholic;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isAlcoholic() {
		return isAlcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		isAlcoholic = alcoholic;
	}

	@Override
	public int compareTo(Drink o) {
		int priceCompare=  Double.compare(o.getPrice(),getPrice());
		return priceCompare==0 && isAlcoholic?-1:priceCompare;
	}
}
