package kdg.prog12.javafx; /**
 * Author: Jan de Rijke
 */

import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class OnStage extends Application{

public static void main(String[]args){
	launch(args);
	}

@Override
public void start(Stage primaryStage){
	Label lblHello = new Label("Hello World!");
	BorderPane root = new BorderPane(lblHello);
	Scene scene = new Scene(root);
	scene.setCursor(Cursor.CROSSHAIR);
	primaryStage.setScene(scene);
	primaryStage.setWidth(500);
	primaryStage.setHeight(600);
	// primaryStage.setResizable(false);
	primaryStage.setTitle("Main stage: headliners until 1am");
	primaryStage.setMaxWidth(700);//only has effect if resizable = true
	primaryStage.show();
	Stage nextStage = new Stage();
	nextStage.setTitle("Freedom stage: The second main stage ");
	Scene groupScene = new Scene(new Group());
	groupScene.setFill(Color.YELLOW);
	nextStage.setScene(groupScene);
	nextStage.show();
	//  You can retrieve the size of the scene but you can not set it.
	//the size of the scene depends on the stage.

	}
	}
