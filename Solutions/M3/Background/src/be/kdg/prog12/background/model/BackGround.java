package be.kdg.prog12.background.model;


import javafx.scene.paint.Color;

import java.util.Random;

public class BackGround {

	private String background="cornsilk";
	private int COLOR_MAX=256;


	private final  Random random = new Random();

	public String getBackground() {
		return background;
	}


	public void  setRandomColor(){
		background =String.format("rgb(%d,%d,%d)",random.nextInt(COLOR_MAX), random.nextInt(COLOR_MAX), random.nextInt(COLOR_MAX));
	}

}
