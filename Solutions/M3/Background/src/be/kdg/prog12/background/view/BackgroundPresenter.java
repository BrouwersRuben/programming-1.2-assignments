package be.kdg.prog12.background.view;

import be.kdg.prog12.background.model.BackGround;

public class BackgroundPresenter {
		private BackGround model;
		private BackgroundView view;

		public BackgroundPresenter(BackGround model, BackgroundView view) {
			this.model = model;
			this.view = view;
			addEventHandlers();
			updateView();
		}

		private void addEventHandlers() {
			view.getBackgroundButton().setOnAction(event -> {
				model.setRandomColor();
				updateView();
			});
		}

		private void updateView() {
			view.setStyle(String.format("-fx-background-color:%s;", model.getBackground()));
		}
}
