package be.kdg.prog12.background;

import be.kdg.prog12.background.model.BackGround;
import be.kdg.prog12.background.view.BackgroundPresenter;
import be.kdg.prog12.background.view.BackgroundView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		BackGround model = new BackGround();
		BackgroundView view = new BackgroundView();
		new BackgroundPresenter(model, view);
		primaryStage.setScene(new Scene(view));
		primaryStage.setWidth(400);
		primaryStage.setHeight(250);
		primaryStage.setTitle("background");
		primaryStage.show();
	}
}