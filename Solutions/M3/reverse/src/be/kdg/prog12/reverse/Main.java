package be.kdg.prog12.reverse;

import be.kdg.prog12.reverse.model.Reverse;
import be.kdg.prog12.reverse.view.ReversePresenter;
import be.kdg.prog12.reverse.view.ReverseView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Reverse model = new Reverse("Enter a text here");
		ReverseView view = new ReverseView();
		new ReversePresenter(model, view);
		primaryStage.setScene(new Scene(view));
		primaryStage.show();
	}
}