package be.kdg.prog12.reverse.view;

import be.kdg.prog12.reverse.model.Reverse;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ReversePresenter {
	private Reverse model;
	private ReverseView view;

	public ReversePresenter(Reverse model, ReverseView view) {
		this.model = model;
		this.view = view;
		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getReverseButton().setOnAction(new ReverseHandler());
	}

	private void updateView() {
		view.getInput().setText(model.getText());
	}

	class ReverseHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			model.setText(view.getInput().getText());
			model.reverse();
			updateView();
		}
	}
}
