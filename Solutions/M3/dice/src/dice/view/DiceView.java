package dice.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;


public class DiceView extends GridPane {
    private ImageView die1;
    private ImageView die2;
    private Button btnThrow;

    public DiceView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        die1 = new ImageView();
        die2 = new ImageView();
        btnThrow = new Button("Roll");
        btnThrow.setPrefWidth(80);
    }

    private void layoutNodes() {
        //setGridLinesVisible(true);
        setPadding(new Insets(10));
        setHgap(10);
        setVgap(10);

        add(die1, 0, 0);
        add(die2, 1, 0);
        add(btnThrow, 0, 1, 2, 1);
        setHalignment(btnThrow, HPos.CENTER);
    }

    ImageView getDie1() {
        return die1;
    }

    ImageView getDie2() {
        return die2;
    }

    Button getBtnThrow() {
        return btnThrow;
    }
}
