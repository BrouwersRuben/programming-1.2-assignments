package dice.view;

import dice.model.Dice;
import javafx.scene.image.Image;

public class Presenter {
    private static final String IMAGE_PACKAGE = "/images";

    private Dice model;
    private DiceView view;

    public Presenter(Dice model, DiceView view) {
        this.model = model;
        this.view = view;
        updateView();
        addEventHandlers();
    }

    private void updateView() {
        view.getDie1().setImage(new Image(IMAGE_PACKAGE + "/die" + model.getNumberofPips1() + ".png"));
        view.getDie2().setImage(new Image(IMAGE_PACKAGE + "/die" + model.getNumberOfPips2() + ".png"));
    }

    //C:\Users\Ruben\OneDrive - Karel de Grote Hogeschool\2020-2021\Classes\Semester 2\Programming 1.2\programming-1.2-assignments\Solutions\dice\rersources\images\die1.png

    private void addEventHandlers() {
        view.getBtnThrow().setOnAction( event-> {
                model.roll();
                updateView();
            }
        );
    }

}
