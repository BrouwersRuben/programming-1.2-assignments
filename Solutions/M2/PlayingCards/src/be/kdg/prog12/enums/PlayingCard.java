package be.kdg.prog12.enums;

import java.util.Random;

public class PlayingCard {
    public enum Rank {
        TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10),
        JACK(10), QUEEN(10), KING(10), ACE(1);

        private final int value;

        Rank(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    public enum Suit {
        HEARTS, DIAMONDS, SPADES, CLUBS;

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    private static final Random random = new Random();

    private final Rank rank;
    private final Suit suit;

    public PlayingCard(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public int getValue() {
        return rank.getValue();
    }

    public static PlayingCard generateRandomPlayingCard() {
        return new PlayingCard(
                Rank.values()[random.nextInt(Rank.values().length)],
                Suit.values()[random.nextInt(Suit.values().length)]
        );
    }

    @Override
    public String toString() {
        return rank + " of " + suit;
    }
}
