package be.kdg.prog12.exceptions;

public class FibonacciException extends ArithmeticException {
    public FibonacciException(String s) {
        super(s);
    }
}
