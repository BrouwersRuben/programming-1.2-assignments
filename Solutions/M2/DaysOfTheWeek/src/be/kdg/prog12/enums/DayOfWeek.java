package be.kdg.prog12.enums;

public enum DayOfWeek {
    MONDAY(true),
    TUESDAY(true),
    WEDNESDAY(true),
    THURSDAY(true),
    FRIDAY(true),
    SATURDAY(false),
    SUNDAY(false);

    private final boolean isWeekday;

    DayOfWeek(boolean isWeekday) {
        this.isWeekday = isWeekday;
    }

    public boolean isWeekday() {
        return isWeekday;
    }

    @Override
    public String toString() {
        return String.format("%s (day %d, weekday = %b)",
                name().substring(0,1).toUpperCase() + name().substring(1).toLowerCase(),
                ordinal() + 1,
                isWeekday);
    }
}
