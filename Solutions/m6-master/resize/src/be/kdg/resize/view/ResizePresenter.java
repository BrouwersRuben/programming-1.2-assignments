package be.kdg.resize.view;

import be.kdg.resize.model.Settings;
import javafx.stage.Window;

import java.util.HashMap;
import java.util.Map;

public class ResizePresenter {
    public enum Properties {X, Y, HEIGHT, WIDTH}

    private static final double DEFAULT_X = 500.0;
    private static final double DEFAULT_Y = 250.0;
    private static final double DEFAULT_WIDTH = 600.0;
    private static final double DEFAULT_HEIGHT = 400.0;

    private final ResizeView view;

    public ResizePresenter(ResizeView view) {
        this.view = view;
    }

    /*private void addEventHandlers() {
    }*/

    /*private void updateView() {
    }*/

    public void addWindowEventHandlers() {
        Window window = view.getScene().getWindow();
        window.setOnCloseRequest(event -> {
            Map<String, Double> props = new HashMap<>();
            props.put(Properties.X.name(), window.getX());
            props.put(Properties.Y.name(), window.getY());
            props.put(Properties.WIDTH.name(), window.getWidth());
            props.put(Properties.HEIGHT.name(), window.getHeight());
            Settings.saveSettings(props);
        });
    }

    public void updateWindowSize() {
        Window window = view.getScene().getWindow();
        final Map<String, Double> windowProps = Settings.loadSettings();
        if (windowProps != null) {
            window.setX(windowProps.get(Properties.X.name()));
            window.setY(windowProps.get(Properties.Y.name()));
            window.setWidth(windowProps.get(Properties.WIDTH.name()));
            window.setHeight(windowProps.get(Properties.HEIGHT.name()));
        } else {
            window.setX(DEFAULT_X);
            window.setY(DEFAULT_Y);
            window.setWidth(DEFAULT_WIDTH);
            window.setHeight(DEFAULT_HEIGHT);
        }
    }
}
