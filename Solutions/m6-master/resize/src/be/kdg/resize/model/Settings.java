package be.kdg.resize.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

public class Settings {
    private static final File SETTINGS_FILE = new File("settings.txt");

    public static void saveSettings(Map<String, Double> settings) {
        try (Formatter formatter = new Formatter(SETTINGS_FILE)) {
            for (Map.Entry<String, Double> entry : settings.entrySet()) {
                formatter.format("%s=%.0f%n", entry.getKey(), entry.getValue());
            }
        } catch (IOException exc) {
            // OK to handle the exception here and not in the client IF saving the window size is considered
            // as not being a major functionality of the client application
            exc.printStackTrace();
        }
    }

    public static Map<String, Double> loadSettings() {
        Map<String, Double> props = new HashMap<>();
        try (FileReader fileReader = new FileReader(SETTINGS_FILE)) {
            final char[] buffer = new char[100];
            fileReader.read(buffer);

            String[] lines = new String(buffer).trim().split("\n");
            for (String line : lines) {
                String[] tokens = line.split("=");
                String key = tokens[0];
                Double value = Double.parseDouble(tokens[1]);
                props.put(key, value);
            }

            return props;
        } catch (Exception exc) {
            return null;
        }
    }
}
