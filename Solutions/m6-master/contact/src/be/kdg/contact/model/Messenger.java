package be.kdg.contact.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Messenger {
    public void send(Message message) throws IOException {
        save(message);
    }

    private void save(Message message) throws IOException {
        PrintWriter os = null;
        try {
            os = new PrintWriter(new BufferedWriter(new FileWriter(
                    String.format("%s_%s.txt",
                            message.getLastName(),
                            message.getFirstName()))));
            os.format("Last name:    %s %n", message.getLastName());
            os.format("First name:   %s %n", message.getFirstName());
            os.format("Email:        %s %n", message.getEmail());
            os.format("Message body: %n%s", message.getMessageBody());
        } finally {
            if (os != null) {
                os.close();
            }
        }
    }
}
