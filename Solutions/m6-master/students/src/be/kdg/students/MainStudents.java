package be.kdg.students;

import be.kdg.students.model.StudentCatalog;
import be.kdg.students.view.Presenter;
import be.kdg.students.view.StudentView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainStudents extends Application {
    @Override
    public void start(Stage primaryStage) {
        StudentCatalog model = new StudentCatalog();
        StudentView view = new StudentView();
        new Presenter(model, view);
        Scene scene = new Scene(view);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Students");
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
