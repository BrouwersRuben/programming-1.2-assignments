package be.kdg.time.view;

import be.kdg.time.model.TimeModel;
import javafx.stage.Window;

import java.time.LocalTime;

public class Presenter {
    private TimeModel model;
    private TimeView view;

    public Presenter(TimeModel model, TimeView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers(){
        this.view.getSlider().setOnMouseDragged(event -> {
            double sliderValue = this.view.getSlider().getValue();

            String sliderValueString = Double.toString(sliderValue);
            double hours = Double.valueOf(sliderValueString.substring(0, sliderValueString.indexOf(".")));
            double minutes = Double.valueOf(sliderValueString.substring(sliderValueString.indexOf("."), sliderValueString.length()))*60;
            LocalTime time = LocalTime.of((int) hours, (int) minutes);

            this.model.setCurrentTime(time);
            updateView();

        });
    }

    private void updateView(){
        view.applyDaylightSun(model.getDaylightPercentage(), model.getSunHeight(), model.getSunPositionX());

    }

    public void addWindowEventHandlers () {
        Window window = view.getScene().getWindow();
        // Add event handlers to window
    }
}
