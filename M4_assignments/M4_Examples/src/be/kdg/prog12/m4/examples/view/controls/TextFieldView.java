package be.kdg.prog12.m4.examples.view.controls;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class TextFieldView extends BorderPane {
    private Label label;
    private TextField name;

    public TextFieldView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.label = new Label("Name:");
        this.name = new TextField();
    }

    private void layoutNodes() {
        BorderPane.setMargin(this.label, new Insets(10));
        BorderPane.setMargin(this.name, new Insets(10));
        this.setLeft(this.label);
        this.setRight(this.name);
    }
}
