package be.kdg.prog12.m4.examples.view.controls;

import javafx.geometry.Insets;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;

public class MenuBarView extends BorderPane {
    private MenuItem exit;

    public MenuBarView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.exit = new MenuItem("Exit");
    }

    private void layoutNodes() {
        final Menu fileMenu = new Menu("File", null, this.exit);
        final MenuBar menuBar = new MenuBar(fileMenu);
        this.setTop(menuBar);
        BorderPane.setMargin(menuBar, new Insets(0.0, 0.0, 100.0, 0.0));
    }
}
