package be.kdg.prog12.m4.examples.view.controls;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class ImageViewView extends BorderPane {
    private ImageView imageView;

    public ImageViewView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.imageView = new ImageView(new Image("/angrybird.png"));
    }

    private void layoutNodes() {
        this.setCenter(this.imageView);
        BorderPane.setMargin(this.imageView, new Insets(10));
    }
}
