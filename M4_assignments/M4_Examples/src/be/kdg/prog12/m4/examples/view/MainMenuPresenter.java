package be.kdg.prog12.m4.examples.view;

import be.kdg.prog12.m4.examples.view.controls.ButtonView;
import be.kdg.prog12.m4.examples.view.controls.CheckBoxView;
import be.kdg.prog12.m4.examples.view.controls.ComboBoxView;
import be.kdg.prog12.m4.examples.view.controls.ImageViewView;
import be.kdg.prog12.m4.examples.view.controls.LabelView1;
import be.kdg.prog12.m4.examples.view.controls.LabelView2;
import be.kdg.prog12.m4.examples.view.controls.MenuBarView;
import be.kdg.prog12.m4.examples.view.controls.TextFieldView;
import be.kdg.prog12.m4.examples.view.pane.BorderPaneView;
import be.kdg.prog12.m4.examples.view.pane.GridPaneView1;
import be.kdg.prog12.m4.examples.view.pane.GridPaneView2;
import be.kdg.prog12.m4.examples.view.pane.GridPaneView3;
import be.kdg.prog12.m4.examples.view.pane.GridPaneView4;
import be.kdg.prog12.m4.examples.view.pane.GridPaneView5;
import be.kdg.prog12.m4.examples.view.pane.HBoxView;
import be.kdg.prog12.m4.examples.view.pane.VBoxView1;
import be.kdg.prog12.m4.examples.view.pane.VBoxView2;
import be.kdg.prog12.m4.examples.view.pane.VBoxView3;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainMenuPresenter {
    private final MainMenuView view;

    public MainMenuPresenter(MainMenuView view) {
        this.view = view;
        // updateView(); This is a static view
        addEventHandlers();
    }

    /*private void updateView() {
    }*/

    private void addEventHandlers() {
        view.getButtonLabelView1().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new LabelView1()), "LabelView1"));
        view.getButtonLabelView2().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new LabelView2()), "LabelView2"));
        view.getButtonImageViewView().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new ImageViewView()), "ImageViewView"));
        view.getButtonButtonView().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new ButtonView()), "ButtonView"));
        view.getButtonCheckBoxView().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new CheckBoxView()), "CheckBoxView"));
        view.getButtonTextFieldView().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new TextFieldView()), "TextFieldView"));
        view.getButtonComboBoxView().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new ComboBoxView()), "ComboBoxView"));
        view.getButtonMenuBarView().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new MenuBarView()), "MenuBarView"));
        view.getButtonBorderPaneView().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new BorderPaneView()), "BorderPaneView"));
        view.getButtonVBoxView1().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new VBoxView1()), "VBoxView1"));
        view.getButtonVBoxView2().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new VBoxView2()), "VBoxView2"));
        view.getButtonVBoxView3().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new VBoxView3()), "VBoxView3"));
        view.getButtonHBoxView().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new HBoxView()), "HBoxView"));
        view.getButtonGridPaneView1().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new GridPaneView1()), "GridPaneView1"));
        view.getButtonGridPaneView2().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new GridPaneView2()), "GridPaneView2"));
        view.getButtonGridPaneView3().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new GridPaneView3()), "GridPaneView3"));
        view.getButtonGridPaneView4().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new GridPaneView4()), "GridPaneView4"));
        view.getButtonGridPaneView5().setOnAction(createSimpleEventHandlerForNewWindow(new Scene(new GridPaneView5()), "GridPaneView5"));
    }

    private EventHandler<ActionEvent> createSimpleEventHandlerForNewWindow(Scene scene, String windowTitle) {
        return (event) -> {
            Stage stage = new Stage();
            stage.initOwner(view.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setX(view.getScene().getWindow().getX() + 100);
            stage.setY(view.getScene().getWindow().getY() + 100);
            stage.setTitle(windowTitle);
            stage.showAndWait();
            //stage.sizeToScene();
        };
    }
}
