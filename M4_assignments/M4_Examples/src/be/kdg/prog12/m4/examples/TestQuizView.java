package be.kdg.prog12.m4.examples;

import be.kdg.prog12.m4.examples.view.events.QuizView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TestQuizView extends Application {
    @Override
    public void start(Stage stage) {
        stage.setScene(new Scene(new QuizView()));
        stage.setTitle("Quiz");
        stage.show();
    }
}
