package be.kdg.cityhall.view;

import javafx.geometry.Insets;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class CityHallPane extends VBox {
    private static final Image IMAGE = new Image("/cityhall.jpg");

    private ImageView imageView;
    private RadioButton normal;
    private RadioButton blackAndWhite;
    private RadioButton sepia;
    private ToggleGroup group;

    void resetEffect() {
        this.imageView.setEffect(null);
    }

    void applyBlackAndWhiteEffect() {
        ColorAdjust blackAndWhite = new ColorAdjust();
        blackAndWhite.setSaturation(-1.0);
        this.imageView.setEffect(blackAndWhite);
    }

    void applySepiaEffect() {
        SepiaTone sepiaTone = new SepiaTone();
        sepiaTone.setLevel(0.8);
        this.imageView.setEffect(sepiaTone);
    }

    public CityHallPane() {
        initialiseNodes();
        layoutNodes();
    }
    private void initialiseNodes() {
        // create and configure controls
        // button = new Button("...")
        // label = new Label("...")
        group = new ToggleGroup();

        imageView = new ImageView("/cityhall.jpg");

        normal = new RadioButton("Normal");
        normal.setToggleGroup(group);
        normal.setSelected(true);

        blackAndWhite = new RadioButton("Black and White");
        blackAndWhite.setToggleGroup(group);

        sepia = new RadioButton("Sepia");
        sepia.setToggleGroup(group);
    }

    private void layoutNodes() {
        // add/set … methods
        // Insets, padding, alignment, …
        this.setSpacing(15);
        this.setPadding(new Insets(15,15, 15, 15));
        this.getChildren().addAll(imageView, normal, blackAndWhite, sepia);
    }
// package-private Getters
// for controls used by Presenter
    public RadioButton getNormal() {
        return normal;
    }
    public RadioButton getBlackAndWhite() {
        return blackAndWhite;
    }
    public RadioButton getSepia() {
        return sepia;
    }
    public ToggleGroup getGroup() {
        return group;
    }
}