package be.kdg.birds.view;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class BirdsView extends BorderPane {
    // private Node attributes (controls)
    private MenuBar menubar;
    private Menu menu1;
    private MenuItem menuitem1;
    private CheckBox checkbox1;

    public BirdsView() {
        initialiseNodes();
        layoutNodes();
    }
    private void initialiseNodes() {
// create and configure controls
// button = new Button("...")
// label = new Label("...")
        menubar = new MenuBar();
        menu1 = new Menu("File");
        menuitem1 = new MenuItem("Bird");
        checkbox1 = new CheckBox();
    }
    private void layoutNodes() {
// add/set … methods
// Insets, padding, alignment, …
        this.setTop(menubar);
        menubar.getMenus().add(menu1);
        menu1.setGraphic(new ImageView("/angrybird.png"));
        menu1.getItems().add(menuitem1);
        menuitem1.setGraphic(new ImageView("/angrybird.png"));

        this.setCenter(checkbox1);
        checkbox1.setGraphic(new ImageView("/angrybird.png"));
    }
// package-private Getters
// for controls used by Presenter
}
