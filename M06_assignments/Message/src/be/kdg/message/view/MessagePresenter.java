package be.kdg.message.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.nio.file.Files;

public class MessagePresenter {
    private final MessageView view;

    public MessagePresenter(MessageView view) {
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        final EventHandler<ActionEvent> eventHandler = event -> updateView();
        view.getForegroundPicker().setOnAction(eventHandler);
        view.getBackgroundPicker().setOnAction(eventHandler);

        view.getMessageField().textProperty().addListener((observable, oldValue, newValue) -> updateView());

        view.getLoadMenuItem().setOnAction(event -> {
            // TODO: complete this method
        });

        view.getSaveMenuItem().setOnAction(event -> {
            FileChooser Save = new FileChooser();
            Window window = view.getScene().getWindow();
            File selectedFile = Save.showSaveDialog(window);
            if (!Files.exists()){
                event.consume();
            }
        });
    }

    private void updateView() {
        final String message = view.getMessageField().getText();
        final Color foreground = view.getForegroundPicker().getValue();
        final Color background = view.getBackgroundPicker().getValue();
        view.showMessage(message, foreground, background);
    }
}
