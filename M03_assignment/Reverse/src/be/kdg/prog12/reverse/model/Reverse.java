package be.kdg.prog12.reverse.model;

public class Reverse {
    // private attributes
    private String Text;

    public Reverse(String text) {
        this.Text = text;
    }

    // methods with business logic
    public void reverse() {
        Text = new StringBuilder(Text).reverse().toString();
    }

    // needed getters and setters
    public String getText() {
        return Text;
    }
    public void setText(String text) {
        this.Text = text;
    }
}

