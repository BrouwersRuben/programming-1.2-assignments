package be.kdg.prog12.reverse.view;

import javafx.geometry.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class ReverseView extends GridPane {
    // private Node attributes (controls)
    private Button button;
    private TextField userText;

    public ReverseView () {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        // button = new Button("...")
        // label = new Label("...")

        button = new Button("reverse");
        userText = new TextField();
    }

    private void layoutNodes() {
        // add/set … methodes
        // Insets, padding, alignment, …

        /*
        userText.prefHeight(10);
        userText.prefWidth(80);
         */

        this.setPadding(new Insets(10, 10, 10, 10));
        this.setVgap(10);
        this.setHgap(10);

        this.add(userText, 0, 0);
        this.add(button, 0, 1);

        //this.setGridLinesVisible(false);

        setHalignment(button, HPos.RIGHT);
    }

    // package-private Getters
    // for controls used by Presenter
    protected Button getButton() {
        return button;
    }
    public TextField getUserText() {
        return userText;
    }
}


