package be.kdg.prog12.reverse.view;

import be.kdg.prog12.reverse.model.Reverse;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ReversePresenter {
    private Reverse model;
    private ReverseView view;

    public ReversePresenter(Reverse model, ReverseView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        // Adds event handlers (inner classes or lambdas)
        // to view controls
        // Event handlers: call model methods and
        // update the view.
        this.view.getButton().setOnAction(new ReverseHandler());
    }

    private void updateView() {
        // fills the view with model data
        view.getUserText().setText(model.getText());

    }

    class ReverseHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            model.setText(view.getUserText().getText());
            model.reverse();
            updateView();
        }
    }
}