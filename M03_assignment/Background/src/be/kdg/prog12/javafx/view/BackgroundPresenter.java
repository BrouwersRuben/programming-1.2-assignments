package be.kdg.prog12.javafx.view;

import be.kdg.prog12.javafx.model.Background;

public class BackgroundPresenter {
    private Background model;
    private BackgroundView view;

    public BackgroundPresenter(
            Background model, BackgroundView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        // Adds event handlers (inner classes or lambdas)
        // to view controls
        // Event handlers: call model methods and
        // update the view.

        this.view.getButton().setOnAction(actionEvent -> {
            this.model.setRandomColor();
            this.updateView();
        });

    }

    private void updateView() {
        // fills the view with model data
        view.setStyle(String.format("-fx-background-color: %s;", model.getBackgroundColor())); // replace color with the model backgound
    }
}
