package be.kdg.prog12.javafx.model;

import java.util.Random;

public class Background {
    // private attributes
    private String backgroundColor;
    private final int MAX_COLOR = 256;

    public Background() {
        backgroundColor= "rgb(255,0,0)";
    }

    // methods with business logic
    public void setRandomColor(){
        Random rd = new Random();
        backgroundColor = "rgb("+ rd.nextInt(MAX_COLOR) + "," + rd.nextInt(MAX_COLOR) + "," + rd.nextInt(MAX_COLOR) + ")";
    }

    // needed getters and setters
    public String getBackgroundColor() {
        return backgroundColor;
    }
}
