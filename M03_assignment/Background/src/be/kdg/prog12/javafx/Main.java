package be.kdg.prog12.javafx;


import be.kdg.prog12.javafx.model.Background;
import be.kdg.prog12.javafx.view.BackgroundPresenter;
import be.kdg.prog12.javafx.view.BackgroundView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        Background model = new Background();
        BackgroundView view = new BackgroundView();
        new BackgroundPresenter(model, view);
        primaryStage.setScene(new Scene(view));

        primaryStage.setTitle("3.02 Background");
        primaryStage.setWidth(400.0);
        primaryStage.setHeight(250.0);
        primaryStage.setResizable(true);
        primaryStage.show();
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
}
