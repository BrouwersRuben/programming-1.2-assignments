package be.kdg.dice.view;

import com.sun.prism.Image;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

import javax.swing.text.html.ImageView;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Random;

public class DiceView extends GridPane {
	private Image image1;
	private Image image2;
	private Button button;

	public DiceView() {
		initialiseNodes();
		layoutNodes();
	}

	private void layoutNodes() {
		this.setPadding(new Insets(15.0));
		this.setVgap(10);
		this.setHgap(10);

		//TODO: set the layout.

	}

	private void initialiseNodes() {
		Random rd = new Random();

		button = new Button("Roll");
		button.setPrefWidth(80.0);

		//TODO: Add the images!
		//Image image = new Image();

	}

	protected Image getImage1() {
		return image1;
	}
	protected Image getImage2() {
		return image2;
	}
	protected Button getButton() {
		return button;
	}
}
