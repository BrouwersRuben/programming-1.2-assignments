package be.kdg.prog12.javafx;

import javafx.application.Application;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class OnStage extends Application {

    @Override
    public void start(Stage stage){

        //TASK 1
        Label helloLabel = new Label("Hello World!");
        BorderPane root = new BorderPane(helloLabel);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        //TASK 2
        stage.setHeight(600);
        stage.setWidth(500);
        stage.setResizable(true);
        stage.setTitle("OnStage");
        stage.setMaxWidth(700);

        //TASK 3
        Stage stage2 = new Stage();
        Scene scene2 = new Scene(new Group());
        scene2.setCursor(Cursor.CROSSHAIR);
        scene2.setFill(Color.YELLOW);
        stage2.setScene(scene2);
        stage2.show();
    }
}
