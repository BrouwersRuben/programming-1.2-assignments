package be.kdg.prog12.retire.model;

public class Retirement {

    // private attributes
    private int birthYear;
    private int RETIREMENT_AGE=67;

    public Retirement() {
        // Constructor
    }

    // methods with business logic
    public int getRetirementYear(){
        return birthYear + RETIREMENT_AGE;
    }

    // needed getters and setters
    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
}
