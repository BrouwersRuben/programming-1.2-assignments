package be.kdg.prog12.retire.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class RetirementView extends BorderPane {
    // private Node attributes (controls)

    private TextField inputField;
    private TextField outputField;
    private Button button;

    public RetirementView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        inputField = new TextField("YYYY");
        outputField = new TextField("");
        button = new Button("BirthYear > RetirementYear");
    }

    private void layoutNodes() {
        //TODO: Does not give the preferred layout
        this.setLeft(inputField);
        this.setMargin(inputField, new Insets(10, 10, 10, 10));

        this.setCenter(button);
        this.setMargin(button, new Insets(10, 10, 10, 10));

        this.setRight(outputField);
        this.setMargin(outputField, new Insets(10, 10, 10, 10));

        this.setPadding(new Insets(10, 10, 10, 10));
    }
    // package-private Getters
    // for controls used by Presenter
    public TextField getInputField() {
        return inputField;
    }
    public TextField getOutputField() {
        return outputField;
    }
    public Button getButton() {
        return button;
    }
    public void setOutputField(TextField outputField) {
        this.outputField = outputField;
    }
}