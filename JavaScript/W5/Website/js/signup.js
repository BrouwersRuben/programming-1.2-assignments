const username = document.getElementById("username").value;
const birthday = document.getElementById("birthday").value;
const password = document.getElementById("password").value;
const passwordConf = document.getElementById("confpassword").value;
const agree = document.getElementById("agreeWithEverything");
const submit = document.getElementById("SB");

const userErr = document.getElementById("usernameErr");
const birthdayErr = document.getElementById("birthdayErr");
const passErr = document.getElementById("passErr");
const passConfErr = document.getElementById("passConfErr");
const AgreeErr = document.getElementById("agreeErr");

const nameREGEX = /^[A-z]+[A-z0-9]{3,}$/;
const level1 = /^\d{6,}|\w{6,}$/ //TODO: This does not include for example 123abc, how do i fix that
const level2 = /^[A-z]$/
const level3 = /^\d$/
const level4 = /^[\p{P}\p{S}]$/ //TODO: Check levels regex
const passREFEX = /^[A-z]+[A-z0-9]{3,}$/;
const dateREGEX = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/

let isVerified = true;

const currentDate = new Date();
console.log(currentDate);

function validateUserName(){
    const uname = username;
    console.log(uname);
    if (uname === ""){
        userErr.innerText = "This Field is empty";
        isVerified = false;
    } else if (!nameREGEX.test(username)){
        userErr.innerText = "You seem to have a typo in your username";
        isVerified = false;
    } else {
        userErr.innerText = "";
        isVerified = true;
    }
}

username.addEventListener('focusout', validateUserName);

function validateDate(){
    //TODO: Check if date is in the past
    const dt = birthday;
    const dateParsed = Date.parse(dt);
    const currentDateParsed = Date.parse(currentDate);
    console.log(dt)
    if (dt === ""){
        birthdayErr.innerText = "This Field is empty";
        isVerified = false;
    } else if (!dateREGEX.test(dt)){
        birthdayErr.innerText = "You seem to have a typo in your date";
        isVerified = false;
    } else if (dateParsed >currentDateParsed){
        birthdayErr.innerText = "Your date seems to be in the future, are you a time traveller?";
        isVerified = false;
    } else {
        birthdayErr.innerText = "";
        isVerified = true;
    }
}

function validatePass(){
    const pword = password;
    console.log(pword);
    if (pword === ""){
        passErr.innerText = "This Field is empty, level 0";
        isVerified = false;
    } else if (level1.test(pword)){
        passErr.innerText = "level 1";
        isVerified = true;
    } else if (level2.test(pword)){
        passErr.innerText = "level 2";
        isVerified = true;
    } else if (level3.test(pword)){
        passErr.innerText = "level 3";
        isVerified = true;
    } else if (level4.test(pword)){
        passErr.innerText = "level 4";
        isVerified = true;
    } else if (!passREFEX.test(pword)){
        passErr.innerText = "You seem to have a typo in your password";
        isVerified = false;
    } else {
        passErr.innerText = "";
        isVerified = true;
    }
}

password.addEventListener('focusout', validatePass);

function validatePassConf(){
    const pwordC = passwordConf;
    console.log(pwordC);
    if (pwordC === ""){
        passConfErr.innerText = "This field is empty";
        isVerified = false;
    } else if (password.value != pwordC){
        passConfErr.innerText = "Your passwords do not match";
        isVerified = false;
    } else {
        passConfErr.innerText = "";
        isVerified = true;
    }
}

passwordConf.addEventListener('focusout', validatePassConf);

function validateAgree(){
    const agr = agree.checked;
    console.log(agr);
    if (agr === false){
        isVerified = false;
        AgreeErr.innerText = "You have not agreed to everything";
    } else {
        AgreeErr.innerText = "";
        isVerified = true;
    }
}

agree.addEventListener('focusout', validateAgree);

// const saveLocal = (event) => {
//     const name = username;
//     const pass = password;
    
//     if (isVerified) {
//         localStorage.setItem('username', name);
//         localStorage.setItem('password', pass);
//     } else {
//         event.preventDefault();
//         alert("Please check your info")
//     }
// }

// submit.addEventListener('click', saveLocal);