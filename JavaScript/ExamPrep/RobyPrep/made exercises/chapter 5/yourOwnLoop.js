function loop(startValue, testValue, updateValue, funct){
    for(let i = startValue; testValue(i); i = updateValue(i)){
        funct(i);
    }
}

loop(3, n => n > 0, n => n - 1, console.log);
// → 3
// → 2
// → 1