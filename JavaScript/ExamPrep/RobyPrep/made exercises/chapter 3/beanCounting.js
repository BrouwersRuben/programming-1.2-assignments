function countBs(string){
    string = string.toLowerCase();
    return string.split("b").length-1;
}

function countChar(string, character){
    string = string.toLowerCase();
    return string.split(character).length-1;
}

console.log(countBs("BBC"));
// → 2
console.log(countChar("kakkerlak", "k"));
// → 4