//  # # # # --> uneven row = space followed by hash
// # # # #  --> even ro< = hash followed by space
//  # # # #
// # # # # 
//  # # # #
// # # # # 
//  # # # #
// # # # #

const gridRowSize = 8;
const gridColumnSize = 8;

for(let row = 1; row <= gridRowSize; row++){
    let string = "";
    for(let column = 1; column <= gridColumnSize; column++){
        if((row + column) % 2 == 0){
            string += " ";
        }else{
            string += "#";
        }
    }
    console.log(string);
}

