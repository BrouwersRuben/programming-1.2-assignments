// Sample 1: the 'window.location' object
console.log(`location:          ${location}`); // window.location
console.log(`location.protocol: ${location.protocol}`);
console.log(`location.hostname: ${location.hostname}`);
console.log(`location.port:     ${location.port}`);
console.log(`location.pathname: ${location.pathname}`);
console.log(`location.search:   ${location.search}`);
console.log(`location:          ${location.protocol}//${location.hostname}:${location.port}${location.pathname}${location.search}`);
console.log(location.protocol + '//' + location.hostname + ':' + location.port + location.pathname + location.search);

// Sample 2: the 'URLSearchParams' class
const params = new URLSearchParams(location.search);

// Here, we'll put the values of the URL search parameters in the actual form fields.
for (let param of params) {
    if (param[0] === '_ijt') {
        continue;
    }
    //console.log(param);

    const formElement = document.querySelector(`[name=${param[0]}]`);
    if (formElement instanceof HTMLSelectElement) {
        const optionElement = formElement.querySelector(`option[value=${param[1]}]`);
        optionElement.selected = true;
    } else if (formElement instanceof HTMLInputElement) {
        if (formElement.type === 'checkbox') {
            formElement.checked = param[1] === 'on';
        } else if (formElement.type === 'radio') {
            const radioElement = document.querySelector(`[name=${param[0]}][value=${param[1]}]`);
            radioElement.checked = true;
        } else if (formElement.type !== 'file') {
            formElement.value = param[1];
        }
    }
}

// Sample 3
// We could access form elements by index:
const formElement = document.getElementsByTagName('form')[0];
const password = formElement.elements[1].value;
console.log(`I got your password, it's '${password}'.`);

// Sample 4
// A URL can't contain question marks (except at the start of the query string) and other special characters.
console.log(params.get('name'));

const pattern = /[&?]([^&?=]*)=([^&?=]*)/g;
let nameFromURL;
for (let match of location.search.matchAll(pattern)) {
    console.log(`PARAM: ${match[1]} / VALUE: ${match[2]}`);
    // check if the value of the name parameter contains a special character like '?'
    if (match[1] === 'name' && params.get('name').indexOf('?') !== -1) {
        nameFromURL = match[2];
    }
}

// if the value of the name parameter contains a special character like '?' it is encoded.
// Decode it before using it
if (nameFromURL) {
    console.log(`Name contains a question mark. 'URLSearchParams' converts automatically, but if manually pull the value from the URL, this is what we get: ${nameFromURL}`);
    console.log(`Decoded manually: ${decodeURIComponent(nameFromURL)}`);
}
