const text = `
package be.kdg.programming;
public class Book {
    private String title;
    public String author;
    
    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }
}
`;

const r1 = new RegExp('\\bpublic(\\s+final)?\\s+\\S+\\s+\\S+;');
console.log('Contains public attribute? ' + (r1.test(text) ? 'yes' : 'no'));

const r2 = /\bpublic\s+void\s+set\S+\s*\(/;
console.log('Contains a public setter? ' + (r2.test(text) ? 'yes' : 'no'));
console.log();

const r3 = /\bpublic\s+(void|\S+)\s+([sg]et\S+)\s*\(/g;
const result1 = text.matchAll(r3);
for (let match of result1) {
    console.log('Found getter or setter: ' + match[2]);
}
console.log();

// Non-capturing group (not discussed in Eloquent JavaScript).
const r4 = /\bpublic\s+(?:void|\S+)\s+([sg]et\S+)\s*\(/g;
const result2 = text.matchAll(r4);
for (let match of result2) {
    console.log('Found getter or setter: ' + match[1]);
}
console.log();

// \d is the same as [0-9]
const r5 = /[0-9]+\.\d\d/g // This one works as well: /[0-9]+[.]\d\d/g
const result3 = '15.95 4.90 13.99 45,22 20.0'.matchAll(r5)
for (let match of result3) {
    console.log('Match: ' + match[0]);
}
