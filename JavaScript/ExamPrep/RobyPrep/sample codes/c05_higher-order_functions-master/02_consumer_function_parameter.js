function executeConsumerTwice(someFunction, someArgument) {
    someFunction(someArgument);
    someFunction(someArgument);
}

function consumer(message) {
    console.log(message);
}

executeConsumerTwice(consumer, "Hello!");

// Let's use an existing function, one that we didn't write ourselves:
executeConsumerTwice(console.log, "Logging!");
