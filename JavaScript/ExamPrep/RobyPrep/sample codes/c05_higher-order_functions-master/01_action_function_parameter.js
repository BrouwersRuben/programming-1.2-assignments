function executeActionTwice(someFunction) {
    someFunction();
    someFunction();
}

function action() {
    console.log("Hello!");
}

executeActionTwice(action);
