class Student {
    constructor(name) {
        if (!name) {
            throw new ReferenceError('\'name\' is mandatory!');
        } else if (typeof name !== 'string') {
            throw new TypeError('\'name\' should be string!');
        }
        this._name = name;
    }
}

try {
    const s1 = new Student(null, '123');
    console.log('\'s1\' has been created.');
} catch (e) {
    console.log(`\'s1\' has NOT been created. (${e.message})`);
}
try {
    const s2 = new Student(undefined, '123');
    console.log('\'s2\' has been created.');
} catch (e) {
    console.log(`\'s2\' has NOT been created. (${e.message})`);
}
try {
    const s3 = new Student('', '123');
    console.log('\'s3\' has been created.');
} catch (e) {
    console.log(`\'s3\' has NOT been created. (${e.message})`);
}
try {
    const s4 = new Student(7, '123');
    console.log('\'s4\' has been created.');
} catch (e) {
    console.log(`\'s4\' has NOT been created. (${e.message})`);
}
try {
    const s5 = new Student('Mary', '123');
    console.log('\'s5\' has been created.');
} catch (e) {
    console.log(`\'s5\' has NOT been created. (${e.message})`);
}
console.log();

try {
    // In JavaScript, the recommended exception type is 'Error'.
    // https://developer.mozilla.org/en-US/docs/web/javascript/reference/global_objects/error

    // Don't use anything other than Error or its subclasses!
    throw 'apple'; // Don't do this!
} catch (fruit) {
    console.log(`Thanks for the ${fruit}.`);
}
