class Address {
    constructor(street, houseNr, postalCode, city, country) {
        this._street = street;
        this._houseNr = houseNr;
        this._postalCode = postalCode;
        this._city = city;
        this._country = country;
    }

    get street() {
        return this._street;
    }

    set street(street) {
        this._street = street;
    }

    get houseNr() {
        return this._houseNr;
    }

    set houseNr(houseNr) {
        this._houseNr = houseNr;
    }

    get postalCode() {
        return this._postalCode;
    }

    set postalCode(postalCode) {
        this._postalCode = postalCode;
    }

    get city() {
        return this._city;
    }

    set city(city) {
        this._city = city;
    }

    get country() {
        return this._country;
    }

    set country(country) {
        this._country = country;
    }

    print() {
        console.log(`${this.street} ${this.houseNr}`);
        console.log(`${this.postalCode} ${this.city}`);
        console.log(this.country.toUpperCase());
    }
}

// Calling a constructor:
const campusGroenplaats = new Address('Nationalestraat', 5, 2000, 'Antwerpen', 'Belgium');

campusGroenplaats.print(); // Calling a method
console.log();

campusGroenplaats.houseNr = 1; // Calling a setter
if (campusGroenplaats.city === 'Antwerpen') { // Calling a getter
    campusGroenplaats.country = 'Atlantis'; // Calling a setter
}

campusGroenplaats.print(); // Calling a method
