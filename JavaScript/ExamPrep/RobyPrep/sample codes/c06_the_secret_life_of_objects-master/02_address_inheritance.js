class Address {
    constructor(recipient, postalCode, city, country) {
        this._recipient = recipient;
        this._postalCode = postalCode;
        this._city = city;
        this._country = country;
    }

    get recipient() {
        return this._recipient;
    }

    print() {
        console.log(`${this._postalCode} ${this._city}`);
        console.log(this._country.toUpperCase());
    }
}

class HomeAddress extends Address {
    constructor(recipient, street, houseNr, postalCode, city, country) {
        super(recipient, postalCode, city, country);
        this._street = street;
        this._houseNr = houseNr;
    }

    print() {
        console.log(this.recipient);
        console.log(`${this._street} ${this._houseNr}`);
        super.print();
    }
}

class POBox extends Address {
    static PO_BOX_PREFIX = 'PO Box';

    constructor(recipient, poBoxNr, postalCode, city, country) {
        super(recipient, postalCode, city, country);
        this._poBoxNr = poBoxNr;
    }

    print() {
        console.log(this.recipient);
        console.log(`${POBox.PO_BOX_PREFIX} ${this._poBoxNr}`);
        super.print();
    }
}

const home = new HomeAddress('Karel de Grote University of Applied Sciences and Arts',
    'Nationalestraat', 5, 2000, 'Antwerpen', 'Belgium');
const poBox = new POBox('KdG', 9001, 2000, 'Antwerpen', 'Belgium');

home.print();
console.log();
poBox.print();
console.log();

console.log('Is \'home\' an instance of Address? ' + (home instanceof Address));
console.log('Is \'home\' an instance of HomeAddress? ' + (home instanceof HomeAddress));
console.log('Is \'home\' an instance of POBox? ' + (home instanceof POBox));
