// Sample 1
const p1 = Promise.resolve('Hi');
p1.then(data => console.log('Data: ' + data));
console.log('End of p1');

// Sample 2
const p2 = new Promise((resolve, reject) => {
    // Some algorithm
    //resolve('The result');
    reject('The reject reason');
});

p2.then(result => {
    console.log('Logging result: ' + result);
}).catch(reason => {
    console.log('Logging reject reason: ' + reason);
});

// Sample 3
const p3 = new Promise(resolve => {
    setTimeout(() => {
        resolve('Task is finished after 10 seconds.');
    }, 10000);
});

p3.then(result => console.log(`Async process is finished with result: '${result}'`));
console.log('End of p3');
