function square(x) {
    return x * x;
}
console.log(square(5)); //25
const cube = function(x){
    return x*x*x;
}
console.log(cube(5)); //125
const power = (base, exponent) => {
    let result = 1;
    for (let count = 0; count < exponent; count++) {
        result *= base;
    }
    return result;
};
console.log(power(5,4)); //625
