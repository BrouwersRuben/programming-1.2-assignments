
// all text elements in the body
console.log("** TEXT CONTENT **\n"+
	document.body.textContent);
// all HTML in the body
console.log("** HTML CONTENT **\n"+
	document.body.innerHTML);
// Select the first p under a div.
// Get its only child: this is a text node
// print the text of the text node
console.log("** div > p nodeValue **\n" +
	document.querySelector("div > p").childNodes[0].nodeValue)
// Same result as the previous statement
// Select the first p under a div.
// print all text under it
console.log("** div > p textContent **\n" +
	document.querySelector("div > p").textContent)


// set innerHTML: HTML string is interpreted and all elements are added
// 1. Select the first p under a div
const pInDiv = document.querySelector("div > p")
// 2. Start building an HTML <ul>
let newHTML = "<ul>"
// 3. Get the text under the element and loop over every word
for (const word of pInDiv.textContent.split(" ")){
	// 4. add a <li> for every word
	newHTML += `<li>${word}</li>`
}
// 5. close the <ul> and replace the content of the paragraph with the same words in list
pInDiv.innerHTML = newHTML + "</ul>"


