// function walk(node,level) {
// 	for (const child of node.childNodes) {
// 		let indent = " ".repeat(level);
// 		if (child.nodeType === Node.ELEMENT_NODE) {
// 			console.log(indent+">" + child.tagName);
// 			walk(child,level+1);
// 		} else{
// 			console.log(indent+ "-"+ child.nodeValue+ "-");
// 		}
// 	}
// }

function walk(node,level) {
	node.childNodes.forEach(node => handleNode(node,level))
}

function handleNode(child,level){
		let indent = " ".repeat(level);
		if (child.nodeType === Node.ELEMENT_NODE) {
			console.log(indent+">" + child.tagName);
			walk(child,level+1);
		} else{
			console.log(indent+ "-"+ child.nodeValue+ "-");
		}
}

walk(document.body,0);
