function getLoggingFunction(type) {
    switch (type) {
        case "log":
            return console.log;
        case "info":
            return console.info;
        case "debug":
            return console.debug;
        case "error":
            return console.error;
        default:
            // See CH1 for '`' and '$' syntax.
            // See CH8 for information on Exceptions.
            throw new Error(`I don't support '${type}'!`);
    }
 }
 
 const theFunctionThatINeed = getLoggingFunction("debug");

 theFunctionThatINeed("Functions, functions, functions!");

 const myNumbers = [456, 98, 731, 845, 566, 333];

const evenNumbers = myNumbers.filter(i =>
                                i % 2 === 0);

console.log("Even numbers:");
console.log(evenNumbers);

console.log("Numbers with even indices:");
console.log(myNumbers.filter((value, index) => {
    return index % 2 == 0;
}));

const prices = [5.99, 6.99, 5.50, 12.00, 8.90];

const total = prices.reduce(
        (theTotalSoFar, currentPrice) => {
            return theTotalSoFar + currentPrice;
        }
);

console.log("Total: " + total);

const people = ["Lars", "Jan", "Ann", "Piet", 
        "Tony", "Tom", "Wim", "Dieter", "Antoon", 
        "Charmaine"];

const result = people
    .filter(name => name.length === 3)
    .map(name => name.toUpperCase())
    .reduce((accum, current) =>
                accum + "-" + current);

console.log("Result: " + result);